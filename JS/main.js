$('#create-quote-form-send').submit(function(e){
    e.preventDefault();

    var text = $('#quote').val();
    
    $.get('http://api.creativehandles.com/getRandomColor',function(data){

        var back_color = data.color;
        var remove_dash_back_color;

        if (back_color.indexOf('#') === 0) {
            remove_dash_back_color = back_color.slice(1);
        }
        var text_color=invertHex(remove_dash_back_color);
        
        $('.content').append("<div style="+"background-color:"+back_color+";color:#"+text_color+"><div class="+"content-title"+"> <p>"+text+
            "</p>"+
            "</div>"+
            "</div>");

    }).fail(function(jqXHR){
        $('.content').append("<div style="+"background-color:#6d4298;color:#FFFFFF><div class="+"content-title"+"> <p>"+text+
            "</p>"+
            "</div>"+
            "</div>");

    });

    $(document).attr("title", ""+text+"");
    $('#quote').val('');
    $(".create-quote-form-btn").attr("disabled", true);
    $('#quote').focus();
});

function invertHex(hexnum){
      
    hexnum = hexnum.toUpperCase();
    var splitnum = hexnum.split("");
    var resultnum = "";
    var simplenum = "FEDCBA9876".split("");
    var complexnum = new Array();
    complexnum.A = "5";
    complexnum.B = "4";
    complexnum.C = "3";
    complexnum.D = "2";
    complexnum.E = "1";
    complexnum.F = "0";
      
    for(i=0; i<6; i++){
      if(!isNaN(splitnum[i])) {
        resultnum += simplenum[splitnum[i]]; 
      } else if(complexnum[splitnum[i]]){
        resultnum += complexnum[splitnum[i]]; 
      }
    }
      
    return resultnum;
}

function checkempty(){
    var text = $('#quote').val();

    if(text == ""){
        $(".create-quote-form-btn").attr("disabled", true);
    }else{
        $(".create-quote-form-btn").removeAttr("disabled");
    }
};

quote.addEventListener("keyup",checkempty,false);
quote.addEventListener("keydown",checkempty,false);
quote.addEventListener("click",checkempty,false);